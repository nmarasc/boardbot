# -*- coding: utf-8 -*-
r"""Main module containing the Boardbot class.

The Boardbot class is contained within this module. Any functions or
variables that are deemed crucial to the user are also kept here.

Classes
-------
Boardbot
    Discord bot for showcasing user posts.
"""
import asyncio
import logging

from .clients import BoardbotClient

__all__ = ['Boardbot']

logger = logging.getLogger(__name__)


class Boardbot:
    r"""Discord bot for showcasing user posts.

    Parameters
    ----------
    token
        Discord client bot token
    guild: optional
        Host guild id
    steamkey : optional
        Steam WebAPI key for setting game playing randomly
    tenor : optional
        Tenor API key

    Attributes
    ----------
    token
        Discord client bot token
    loop
        Event loop for bot
    client
        Boardbot discord client instance

    Methods
    -------
    run
        Schedule bot tasks and start the event loop

    Raises
    ------
    ValueError
        No bot token was provided
    """
    _TICK_ROLLOVER = 7200
    _GAME_TIMER = 3600

    def __init__(self, token, guild=None, steamkey=None, tenor=None):
        r"""Boardbot initialization."""

        self._ticks = 0

        if not token:
            logger.critical('No token was provided!')
            raise ValueError('no token provided')

        self.token = token
        self.loop = asyncio.get_event_loop()
        self.client = BoardbotClient(guild, steamkey, tenor)

        logger.info('Discord client created')

    def run(self):
        r"""Boardbot main loop.

        Create and execute event tasks. One for the discord client and a
        tick task for timer based events.
        """
        self._running = True

        self.loop.create_task(self.client.start(self.token))
        logger.info('Discord client task created')

        self.loop.create_task(self._tick())
        logger.info('Boardbot tick task created')

        try:
            logger.info('Executing main event loop')
            self.loop.run_forever()
        except KeyboardInterrupt:
            logger.info('Keyboard interrupt detected')
        finally:
            logger.info('Stopping tasks now')
            self._running = False
            self.loop.run_until_complete(self.client.close())
            tasks = asyncio.all_tasks(self.loop)
            for task in tasks:
                task.cancel()
            self.loop.run_until_complete(asyncio.gather(*tasks))
            self.loop.stop()
        self.loop.close()

    async def _tick(self):
        r"""Boardbot timed event handler."""
        await self.client.wait_until_ready()

        while self._running:
            self._ticks = (self._ticks + 1) % self._TICK_ROLLOVER

            try:
                if self._ticks % self._GAME_TIMER == 0:
                    await self.client.on_game_change()

                await asyncio.sleep(1)
            except asyncio.CancelledError:
                logger.warning('Task cancelled: _tick')
