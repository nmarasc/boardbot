# -*- coding: utf-8 -*-
r"""Boardbot client classes module.

API client extention classes for Boardbot.

Classes
-------
BoardbotClient
    Discord client extension
"""
import logging
import random

import discord
from mongoengine import connect
from pymongo.errors import ServerSelectionTimeoutError

from .config import ConfigManager
from .util.database import App, buildGameDB
from .util.share import share, alreadyShared

logger = logging.getLogger(__name__)


class BoardbotClient(discord.Bot):
    r"""Boardbot implementation of the Discord client.

    Parameters
    ----------
    guild
        Host guild id
    steamkey
        Steam WebAPI key
    tenor
        Tenor API key

    Methods
    -------
    on_ready
        Called when successfully logged into client
    on_guild_join
        Add guild to the database when joining a new one
    on_reaction_add
        Check for voting emoji and share post if needed
    on_message
        Check messages received
    on_game_change
        Called when a game change event occurs
    """
    def __init__(self, guild, steamkey, tenor):
        r"""Boardbot client initialization."""
        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__(intents=intents)

        try:
            connect('guilds', alias='dbguilds')
        except ServerSelectionTimeoutError:
            logger.error('Guild server unreachable!')
        try:
            connect('messages', alias='dbmessages')
        except ServerSelectionTimeoutError:
            logger.error('Message server unavailable!')

        try:
            connect('games', alias='dbgames')
            if len(App.objects) == 0:
                buildGameDB(steamkey)
            self.db = True
        except ServerSelectionTimeoutError:
            logger.warning('Mongodb not available, cannot build games list')
            self.db = False

        self.config = ConfigManager(self)
        self.guild = guild
        self.tenor = tenor

        self.load_extension('boardbot.cogs.config')

    async def on_ready(self):
        r"""Gather information when logged into client."""
        self.guild = self.get_guild(int(self.guild))
        # if self.guild:
        #    self.load_extension('boardbot.cogs.stuff')

        for guild in self.guilds:
            self.config.addGuild(guild)

        await self._setGame()

        logger.info(f'Boardbot logged into discord as {self.user}')

    async def on_guild_join(self, guild):
        r"""Add guild to database when joining new one.

        Parameters
        ----------
        guild
            Guild to add to database"""
        self.config.addGuild(guild)
        logger.info(f'Joined new server: {guild.name}')

    async def on_reaction_add(self, reaction, user):
        r"""Check added reaction for votes.

        Parameters
        ----------
        reaction
            current state of the reaction
        user
            user who added the reaction
        """
        if reaction.message.author.id == self.user.id:
            return

        if not alreadyShared(reaction.message.id):
            emoji = self.config.getEmoji(reaction.message.guild)
            threshold = self.config.getThreshold(reaction.message.guild)
            if reaction.emoji == emoji and reaction.count == threshold:
                channel = self.config.getChannel(reaction.message.guild)
                await share(reaction.message, channel)

    async def on_message(self, message):
        r"""Check messages received.

        Parameters
        ----------
        message
            discord message object
        """
        # try:
        #    await self.get_cog('Stuff').on_message(message)
        # except AttributeError:
        #    pass
        pass

    async def on_game_change(self):
        r"""Change game status event handler."""
        logger.info('Game change event triggered')
        await self._setGame()

    async def _setGame(self):
        r"""Set a new game status."""
        if self.db:
            playing = discord.Game(random.choice(App.objects).name)
            await self.change_presence(activity=playing)
        else:
            await self.change_presence(activity=discord.Game('if you see this borpaSpin'))
