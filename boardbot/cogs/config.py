# -*- coding: utf-8 -*-
r"""Guild configuration module.

Classes
-------
Config
    Guild configuration
"""
import logging

import discord
from discord.ext import commands

from ..ui.config import ConfigUI

logger = logging.getLogger(__name__)

__all__ = ['Config']


class Config(commands.Cog):
    r"""Guild configuration.

    Methods
    -------
    setchannel
        Set channel to use for board
    setemoji
        Set emoji to use for board voting
    setthreshold
        Set threshold for board voting
    config
        Open the configuration window
    getchannel
        Get channel being used for board
    getemoji
        Get emoji being used for board voting
    getthreshold
        Get threshold for board voting
    getsettings
        Get all settings values
    """
    def __init__(self, bot):
        self.bot = bot

    @discord.slash_command(
        description='Set channel to use for board',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def setchannel(self, ctx, channel: discord.Option(str)):
        r"""Set channel to use for board.

        Parameters
        ----------
        ctx
            Command context information
        channel
            channel to set as board
        """
        success, channel = self.bot.config.setChannel(ctx.guild, channel)
        if success:
            await ctx.respond(f'Set {channel} as the board channel')
        else:
            await ctx.respond(f'{channel} is not a valid channel from this server, please try again')

    @discord.slash_command(
        description='Set emoji to use for board voting',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def setemoji(self, ctx, emoji: discord.Option(str)):
        r"""Set emoji to use for board voting.

        Parameters
        ----------
        ctx
            Command context information
        emoji
            emoji to use for board voting
        """
        success, emoji = self.bot.config.setEmoji(ctx.guild, emoji)
        if success:
            await ctx.respond(f'Set {emoji} as the voting emoji')
        else:
            await ctx.respond(f'{emoji} is not a valid emoji from this server, please try again')

    @discord.slash_command(
        description='Set threshold for board voting',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def setthreshold(self, ctx, threshold: discord.Option(int)):
        r"""Set threshold for board voting.

        Parameters
        ----------
        ctx
            Command context information
        threshold
            Value to set as board voting threshold
        """
        success = self.bot.config.setThreshold(ctx.guild, threshold)
        if success:
            await ctx.respond(f'Set {threshold} as the voting threshold')
        else:
            await ctx.respond('Threshold value must be an integer greater than 0')

    @discord.slash_command(
        description='Open configuration window',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def config(self, ctx):
        r"""Open the configuration window.

        Parameter
        ---------
        ctx
            Command context information
        """
        modal = ConfigUI(self.bot, title='Boardbot Configuration')
        await ctx.send_modal(modal)

    @discord.slash_command(
        description='Get channel being used for board',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def getchannel(self, ctx):
        r"""Get channel being used for board.

        Parameters
        ----------
        ctx
            Command context information
        """
        channel = self.bot.config.getChannel(ctx.guild)
        try:
            await ctx.respond(f'The board channel is {channel.mention}')
        except AttributeError:
            await ctx.respond('The board channel is not configured, please use /setchannel')

    @discord.slash_command(
        description='Get emoji being used for board voting',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def getemoji(self, ctx):
        r"""Get emoji being used for board voting.

        Parameters
        ----------
        ctx
            Command context information
        """
        emoji = self.bot.config.getEmoji(ctx.guild)
        if emoji:
            await ctx.respond(f'The board emoji is {emoji}')
        else:
            await ctx.respond('The board emoji is not configured, please use /setemoji')

    @discord.slash_command(
        description='Get threshold value for board voting',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def getthreshold(self, ctx):
        r"""Get threshold value for board voting.

        Parameters
        ----------
        ctx
            Command context information
        """
        threshold = self.bot.config.getThreshold(ctx.guild)
        if threshold > 0:
            await ctx.respond(f'The voting threshold is {threshold}')
        else:
            await ctx.respond('The voting threshold is not configured, please use /setthreshold')

    @discord.slash_command(
        description='Get all settings values',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def getsettings(self, ctx):
        r"""Get all settings values for a guild.

        Parameters
        ----------
        ctx
            Command context information
        """
        channel = self.bot.config.getChannel(ctx.guild)
        emoji = self.bot.config.getEmoji(ctx.guild)
        threshold = self.bot.config.getThreshold(ctx.guild)
        response = 'Boardbot settings:'

        try:
            response += f'\n\tThe board channel is {channel.mention}'
        except AttributeError:
            response += '\n\tThe board channel is not configured, please use /setchannel'
        if emoji:
            response += f'\n\tThe board emoji is {emoji}'
        else:
            response += '\n\tThe board emoji is not configured, please use /setemoji'
        if threshold > 0:
            response += f'\n\tThe voting threshold is {threshold}'
        else:
            response += '\n\tThe voting threshold is not configured, please use /setthreshold'

        await ctx.respond(response)


def setup(bot):
    bot.add_cog(Config(bot))
    logger.info('Loaded Config Cog')
