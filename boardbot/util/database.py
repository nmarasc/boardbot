# -*- coding: utf-8 -*-
r"""Mongoengine document definitions.

Classes
-------
App
    Steam application entry

Functions
---------
buildGameDB
    Create the database of Steam games
"""
import logging

from mongoengine import Document
from mongoengine.fields import IntField, StringField, BooleanField
from steam.webapi import WebAPI

logger = logging.getLogger(__name__)


class App(Document):
    meta = {'db_alias': 'dbgames'}
    appid = IntField(required=True)
    name = StringField(required=True)


class Guild(Document):
    meta = {'db_alias': 'dbguilds'}
    gid = IntField(required=True)
    name = StringField(required=True)
    channel = IntField(required=True)
    emoji = StringField(required=True)
    threshold = IntField(required=True)
    stuff = BooleanField(required=True)


class Message(Document):
    meta = {'db_alias': 'dbmessages'}
    mid = IntField(require=True)


def buildGameDB(steamkey):
    r"""Build the database of games.

    Parameters
    ----------
    steamkey
        Steam WebAPI key for fetching game list
    """
    logger.info('Building game database')
    steamAPI = WebAPI(key=steamkey)
    try:
        service = steamAPI.IStoreService
    except AttributeError:
        logger.warning('Steam WebAPI key not provided or service unavailable')
        entry = App(appid=1, name='if you see this borpaSpin')
        entry.save()
        return
    response = service.GetAppList(include_games=1, max_results=50000)['response']
    appid = 1
    for app in response['apps']:
        entry = App(appid=appid, name=app['name'])
        entry.save()
        appid += 1
