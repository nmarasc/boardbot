# -*- coding: utf-8 -*-

import logging

from boardbot.__version__ import * # NOQA
from boardbot.core import Boardbot, __doc__ # NOQA

try:
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass

# Avoids "No handler found" warning when logging is not configured
logging.getLogger(__name__).addHandler(NullHandler())
