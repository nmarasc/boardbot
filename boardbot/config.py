# -*- coding: utf-8 -*-
r"""Module container configuration classes.

Classes
-------
Config
    Configuration management class
"""
import logging
import re

import discord
from mongoengine import DoesNotExist
from pymongo.errors import ServerSelectionTimeoutError
from emoji import emojize, is_emoji

from .util.database import Guild

__all__ = ['ConfigManager']

logger = logging.getLogger(__name__)


class ConfigManager():
    r"""Configuration management class.

    Methods
    -------
    addGuild
        Add guild to database if needed
    setChannel
        Set the board channel for a guild
    setEmoji
        Set the voting emoji for a guild
    setThreshold
        Set the threshold value for a guild
    setStuff
        Toggle stuff reactions for a guild
    getChannel
        Get the channel being used for a board
    getEmoji
        Get the emoji being used for board voting
    getThreshold
        Get the voting threshold for a guild
    """
    def __init__(self, bot):
        self.bot = bot

    def addGuild(self, guild):
        r"""Add guild to configuration.

        Parameters
        ----------
        guild
            discord guild object to add
        """
        try:
            Guild.objects.get(gid=guild.id)
        except DoesNotExist:
            entry = Guild(gid=guild.id, name=guild.name, channel=0,
                          emoji='', threshold=0, stuff=False)
            entry.save()
        except ServerSelectionTimeoutError:
            logger.error('Guild server not reachable!')

    def setChannel(self, guild, channel):
        r"""Set the board channel for a guild.

        Parameters
        ----------
        guild
            Guild object to set channel of
        channel
            Channel name to try and set

        Returns
        -------
        bool
            True if channel was set successfully
        str
            Printable channel on success or original channel value on failure
        """
        ch = re.search(r'<#(\d+)>', channel)
        try:
            # Ensure id received is a valid channel
            ch = guild.get_channel(int(ch.group(1)))
        except AttributeError:
            ch = discord.utils.get(guild.channels, name=channel.replace('#', ''))
        try:
            Guild.objects.get(gid=guild.id).update(set__channel=ch.id)
            return True, ch.mention
        except AttributeError:
            return False, channel

    def setEmoji(self, guild, emoji):
        r"""Set the voting emoji for a guild.

        Parameters
        ----------
        guild
            Guild object to set voting emoji of
        emoji
            Emoji to try and set

        Returns
        -------
        bool
            True if emoji set successfully
        str
            Printable emoji on success or original emoji value on failure
        """
        if is_emoji(emojize(emoji, language='alias')):
            e = emojize(emoji, language='alias')
            Guild.objects.get(gid=guild.id).update(set__emoji=e)
            return True, e

        try:
            e = re.search(r'.*:(\S+):.*', emoji).group(1)
        except AttributeError:
            e = emoji

        e = discord.utils.get(guild.emojis, name=e)
        try:
            Guild.objects.get(gid=guild.id).update(set__emoji=e.name)
            return True, e
        except AttributeError:
            return False, emoji

    def setThreshold(self, guild, threshold):
        r"""Set the threshold value for a guild.

        Parameters
        ----------
        guild
            Guild object to set threshold of
        threshold
            Value to try and set

        Returns
        -------
        bool
            True if threshold set successfully
        """
        try:
            if int(threshold) > 0:
                Guild.objects.get(gid=guild.id).update(set__threshold=int(threshold))
                return True
            else:
                return False
        except ValueError:
            return False

    def setStuff(self, guild):
        r"""Toggle stuff reactions for a guild.

        Parameters
        ----------
        guild
            Guild object to toggle

        Returns
        -------
        bool
            True if reactions have been enabled
        """
        try:
            stuff = Guild.objects.get(gid=guild.id).stuff
            Guild.objects.get(gid=guild.id).update(set__stuff=not stuff)
            return not stuff
        except DoesNotExist:
            Guild.objects.get(gid=guild.id).update(set__stuff=True)
            return True

    def getChannel(self, guild):
        r"""Get the channel being used for a board.

        Parameters
        ----------
        guild
            Guild object to get the board channel of

        Returns
        -------
        channel
            Discord channel object or None if not found
        """
        return self.bot.get_channel(Guild.objects.get(gid=guild.id).channel)

    def getEmoji(self, guild):
        r"""Get the emoji being used for board voting.

        Parameters
        ----------
        guild
            Guild object to get the board emoji of

        Returns
        -------
        emoji
            Discord emoji, unicode string or None
        """
        emoji = Guild.objects.get(gid=guild.id).emoji
        return emoji if is_emoji(emoji) else discord.utils.get(guild.emojis, name=emoji)

    def getThreshold(self, guild):
        r"""Get the voting threshold for a guild.

        Parameters
        ----------
        guild
            Guild object to get threshold value of

        Returns
        -------
        int
            Threshold value for the guild
        """
        return Guild.objects.get(gid=guild.id).threshold

    def getStuff(self, guild):
        r"""Get stuff reaction value for a guild.

        Parameters
        ----------
        guild
            Guild object to get stuff reaction value of

        Returns
        -------
        bool
            stuff reaction value of the guild
        """
        try:
            return Guild.objects.get(gid=guild.id).stuff
        except DoesNotExist:
            return False
