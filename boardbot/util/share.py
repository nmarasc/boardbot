# -*- coding: utf-8 -*-
r"""Boardbot message sharing module.

Classes
-------
ShareEmbed
    Embed extension for sharing messages

Functions
---------
share
    Share the message on the board channel
alreadyShared
    Check if a message was already shared
"""
import logging
from datetime import datetime

import discord
from mongoengine import DoesNotExist

from ..util.database import Message

logger = logging.getLogger(__name__)

__all__ = ['ShareEmbed', 'share', 'alreadyShared']


class ShareEmbed(discord.Embed):
    r"""Embed extension for sharing messages."""
    def __init__(self, message):
        super().__init__(
            color=discord.Color.light_grey(),
            timestamp=datetime.now()
        )

        author = f'{message.author.name}'
        try:
            super().set_author(name=author, icon_url=message.author.avatar.url)
        except AttributeError:
            super().set_author(name=author, icon_url=message.author.default_avatar.url)

        if message.content:
            super().add_field(name='Message', value=message.content, inline=True)

        if message.attachments:
            attachment_names = '\n'.join([f'📎 [{a.filename}]({a.url})' for a in message.attachments])
            super().add_field(name='Attachments', value=attachment_names, inline=False)
            if message.attachments[0].content_type.startswith('image/'):
                super().set_image(url=message.attachments[0].url)
        elif message.embeds:
            super().set_image(url=message.embeds[0].url)

        super().add_field(
            name='Original message',
            value=f'\n\n➡️ [link]({message.jump_url}) from {message.channel.mention}',
            inline=False
        )


async def share(message, channel):
    r"""Share a message on the board channel.

    Parameters
    ----------
    message
        Message to send
    channel
        Channel to send the message to"""
    entry = Message(mid=message.id)
    entry.save()
    await channel.send(embeds=[ShareEmbed(message)])


def alreadyShared(message):
    r"""Check if a message was already shared on the board channel.

    Parameters
    ----------
    message
        Message to check if it was shared
    """
    try:
        Message.objects.get(mid=message)
        return True
    except DoesNotExist:
        return False
