## [Unreleased/Planned]

- Showcase high scores/leaderboard
- Embed tenor links, and youtube thumbnails

## [0.3.0] - 2024-02-18

### Added
-

### Changed
- Config menu values are optional
- Refactored set command paths
- Refactored get command paths
- Updated py-cord version

### Removed
- Message author discriminator from posts

## [0.2.0] - 2022-06-07

### Added
- Embed attachment image in shared post

## [0.1.0] - 2022-06-06

### Added
- Slash command: setchannel
- Slash command: setemoji
- Slash command: setthreshold
- Slash command: config
- Slash command: getchannel
- Slash command: getemoji
- Slash command: getthreshold
- Slash command: getsettings
- User voting and cross posting of messages
- Auto rotating playing status of steam games