# -*- coding: utf-8 -*-

__all__ = ['__title__', '__description__', '__url__',
           '__version__', '__author__', '__author_email__',
           '__license__', '__copyright__']


def getVersion(version):
    r"""Convert version tuple into PEP 440 compliant string.

    Parameters
    ----------
    version
        Version information

    Returns
    -------
    str
        PEP 440 compliant version string, X.Y.Z[{a|b|rc}N]

        a, b, rc for alpha, beta, release contender

        e.g. 1.2.3rc4

    Raises
    ------
    AssertionError
        The fourth value in the version tuple was not a valid string

        Valid strings are 'alpha', 'beta', 'rc', 'final'
    """
    assert version[3] in ('alpha', 'beta', 'rc', 'final')

    # Main version is always of the form X.Y.Z
    main = '.'.join(str(x) for x in version[:3])

    sub = ''
    if version[3] != 'final':
        mapping = {'alpha': 'a', 'beta': 'b', 'rc': 'rc'}
        sub = mapping[version[3]] + str(version[4])

    return main + sub


VERSION = (0, 3, 0, 'final', 0)

__title__ = 'boardbot'
__description__ = 'Discord bot for showcasing user posts.'
__url__ = 'https://gitlab.com/nmarasc/boardbot'
__version__ = getVersion(VERSION)
__author__ = 'Nick Marasco'
__author_email__ = 'nicdmarasco@gmail.com'
__license__ = 'MIT License'
__copyright__ = 'Copyright 2022 Nick Marasco'

if __name__ == '__main__':
    print(getVersion(VERSION))
