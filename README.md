# Boardbot

A discord bot to showcase exceptional user posts through reaction voting and cross posting to a showcase channel.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for code of conduct details and information on the
pull request process.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
