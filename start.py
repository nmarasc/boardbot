#!/usr/bin/env python3.10
# -*- coding: utf-8 -*-
r"""Main driver script for Boardbot.

Command line parsing, logging configuration, and bot starting.
"""
import argparse
import logging
import logging.config
import os

from boardbot import Boardbot

LOGDIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '.log')


def main(logconfig):

    args = parseCommandLine()

    if args.debug:
        logconfig['root']['level'] = 'DEBUG'
    if args.verbose:
        logconfig['root']['handlers'].append('console')

    os.makedirs(LOGDIR, exist_ok=True)
    logging.config.dictConfig(logconfig)
    logger = logging.getLogger(__name__)
    logger.info('Logging configured and initialized')

    token = os.getenv('DISCORD_TOKEN')
    steam = os.getenv('STEAMAPI_KEY')
    tenor = os.getenv('TENORAPI_KEY')
    guild = os.getenv('HOST_GUILD')
    boardbot = Boardbot(token, guild, steam, tenor)
    boardbot.run()


def parseCommandLine():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    parser.add_argument(
        '-d', '--debug', action='store_true',
        help='\tadd debug messages to the log')
    parser.add_argument(
        '-v', '--verbose', action='store_true',
        help='\tprint log messages to the console')

    args = parser.parse_args()
    return args


if __name__ == '__main__':

    logconfig = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(asctime)s [%(levelname)-8s] %(name)s: %(message)s'
            }
        },
        'handlers': {
            'default': {
                'level': 'DEBUG',
                'formatter': 'standard',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': f'{LOGDIR}/boardbot.log',
                'mode': 'a',
                'maxBytes': 10485760,
                'backupCount': 5
            },
            'console': {
                'level': 'DEBUG',
                'formatter': 'standard',
                'class': 'logging.StreamHandler'
            }
        },
        'root': {
            'handlers': ['default'],
            'level': 'INFO'
        }
    }

    main(logconfig)
