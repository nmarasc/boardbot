# -*- coding: utf-8 -*-
r"""Stuff reaction module.

Classes
-------
Stuff
    Stuff reaction cog
"""
import logging
import re

import discord
from discord.ext import commands

logger = logging.getLogger(__name__)

__all__ = ['Stuff']


class Stuff(commands.Cog):
    r"""Stuff reaction cog.

    Methods
    -------
    """
    def __init__(self, bot):
        self.bot = bot

    async def on_message(self, message):
        r"""Check if need to add stuff reaction.

        Parameters
        ----------
        message
            Message to check
        """
        if self.bot.config.getStuff(message.guild) and self._hasStuff(message):
            stuff = discord.utils.get(self.bot.guild.emojis, name='stuff')
            await message.add_reaction(stuff)

    @discord.slash_command(
        description='Toggle stuff reactions',
    )
    @commands.check_any(commands.is_owner(), commands.has_guild_permissions(manage_guild=True))
    async def togglestuff(self, ctx):
        r"""Toggle stuff reactions.

        Parameters
        ----------
        ctx
            Command context information
        """
        await ctx.respond(f'Set stuff reactions: {self.bot.config.setStuff(ctx.guild)}')

    def _hasStuff(message):
        return re.match(r'\bstuff\b', message.content, re.IGNORECASE)


def setup(bot):
    bot.add_cog(Stuff(bot))
    logger.info('Loaded Stuff Cog')
