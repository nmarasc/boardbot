# -*- coding: utf-8 -*-
r"""Configuration modal module.

Classes
-------
ConfigView
    Configuration ui modal
"""
import logging

import discord

logger = logging.getLogger(__name__)


class ConfigUI(discord.ui.Modal):
    def __init__(self, bot, **kwargs):
        super().__init__(**kwargs)
        self.bot = bot
        self.add_item(discord.ui.InputText(label='Channel', required=False))
        self.add_item(discord.ui.InputText(label='Emoji', required=False))
        self.add_item(discord.ui.InputText(label='Threshold', required=False))

    async def callback(self, interaction):
        response = 'Settings successfully processed:'
        errors = ''

        channel = self.children[0].value
        if channel:
            success, channel = self.bot.config.setChannel(interaction.guild, channel)
            if success:
                response += f'\n\tSet {channel} as the board channel'
            else:
                errors += f'\n\t{channel} is not a valid channel from this server, please try again'

        emoji = self.children[1].value
        if emoji:
            success, emoji = self.bot.config.setEmoji(interaction.guild, emoji)
            if success:
                response += f'\n\tSet {emoji} as the voting emoji'
            else:
                errors += f'\n\t{emoji} is not a valid emoji from this server, please try again'

        threshold = self.children[2].value
        if threshold:
            success = self.bot.config.setThreshold(interaction.guild, threshold)
            if success:
                response += f'\n\tSet {threshold} as the voting threshold'
            else:
                errors += '\n\tThreshold value must be an integer greater than 0'

        if errors:
            await interaction.response.send_message(
                f'While processing your settings the following error(s) occurred:{errors}'
            )
        else:
            await interaction.response.send_message(response)
