#!/bin/bash
set -ex

CNAME=$1
URL=$2
ENV=$3

LOG=".log/"
TZ=`cat /etc/timezone`

MOUNT_OPS="$MOUNT_OPS -v $PWD/$LOG:/app/$LOG"

source $PWD/.auth/.creds
docker login $URL -u $GL_USER -p $GL_TOKEN

docker pull $URL:latest
docker run -d --restart unless-stopped --name $CNAME --env-file $3 -e TZ=$TZ --network="host" $MOUNT_OPS $URL
